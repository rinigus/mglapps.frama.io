+ **name:** name of the app
+ **summary:** one or two sentences about the app - normally a quote from the project site
+ **summary source:** if 'summary' is a quote, this provides the source
+ **mobile compatibility:** a number between 0 (best) and 4 (worst) to name the mobile compatibility of the app
    + 0 = perfect - nothing left to be done 
    + 1 = mobile compatibility could be perfect because the app was ported to other mobile platforms (e.g. Android)
    + 2 = some parts of the app are not usable with touch input on small screens
    + 3 = many parts of the app are not usable with touch input on small screens
    + 4 = app is unusable with touch input on small screens – why is this app in the Mobile GNU/Linux Apps app list?
+ **repository:** link to the code repository
+ **website:** link to the project‘s website
+ **screenshots:** links to sites providing screenshots of the application
+ **license:** license of the project
+ **framework:** UI frameworks which are used by the app
+ **more information:** links to sites providing more information about the app
+ **description:** more detailed text about the app - normally a quote from the project site
+ **description source:** if 'description' is a quote, this provides the source
+ **backend:** backends which are used by the app
+ **service:** services the app includes and makes use of
